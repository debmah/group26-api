const fs = require('fs');

function write(fileName, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./files/' + fileName, content, function (err, done) {
            if (err) {
                reject(err);
            } else {
                resolve(done);
            }
        })
    })

}

module.exports = {
    write
};