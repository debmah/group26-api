const express = require('express');

const app = express();
// app is entire express framework
app.get('/', function (req, res) {
    // using express we can complete req-res cycle in many way
    // sending files,
    // sending status code
    // sending json content
    // downloading,
    // redirect,render
    // req is http req object
    // res is http res object
    // res.sendStatus(400)
    require('fs').readFile('sdjklf',function(err,done){
        if(err){
            res.json(err);
        }
    })
    // res.json({
    //     msg: "Welcome to express"
    // })
})
app.post('/', function (req, res) {
    // req is http req object
    // res is http res object
})
app.get('/write', function (req, res) {
    // write
    res.send('perform wirte task');
})
app.get('/write/:name/:content', function (req, res) {
    // write
    // req.params
    res.json({
        msg: "hi from dynamic endpoint",
        params: req.params,
        query:req.query
    })
})
app.get('/read', function (req, res) {
    res.send('perform read task')
    // read
})


app.listen(8080, function (err, done) {
    if (err) {
        console.log('error listening  >', err);
    } else {
        console.log("Server is listening at port 8080");

    }
})


// middleware